import { initializeApp } from "firebase/app";
import "firebase/firestore";

const firebaseConfig = {
    apiKey: "AIzaSyALBdxpnPZt6vTWxCaFgkEArxVFnlFzaus",
    authDomain: "bbe-demo-vue-project.firebaseapp.com",
    projectId: "bbe-demo-vue-project",
    storageBucket: "bbe-demo-vue-project.appspot.com",
    messagingSenderId: "169307817916",
    appId: "1:169307817916:web:143e428dc72fc946de8db9",
    measurementId: "G-48P9SDNVFL"
};

const firebaseApp = initializeApp(firebaseConfig);

export default firebaseApp.firestore();